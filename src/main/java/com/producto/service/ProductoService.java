package com.producto.service;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import com.producto.model.Producto;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.eclipse.microprofile.config.inject.ConfigProperty;

@ApplicationScoped
public class ProductoService {

    @Inject
    MongoClient mongo;

    @ConfigProperty(name = "mongo.database") //Database name configured in application.properties
    String database;

    @ConfigProperty(name = "mongo.collection") //Database collection configured in application.properties
    String collection;

    public List<Producto> listAllProductos() {
        List<Producto> productos = new ArrayList();
        MongoCursor<Document> cursor = getCollection().find().iterator();

        try {
            while (cursor.hasNext()) {
                Document document = cursor.next();
                Producto producto = new Producto();

                producto.setProducto(document.getString("producto"));
                producto.setStock(document.getInteger("stock"));
                producto.setValor(document.getInteger("valor"));
                producto.set_id(document.getObjectId("_id"));

                productos.add(producto);
            }
        } catch (Exception ex) {
            System.out.println("No se puede obtener la lista de productos");
            ex.printStackTrace();
        } finally {
            cursor.close();
        }
        return productos;
    }

    public Producto getProductoById(String id) {
        Document documentProducto = (Document) getCollection().find(Filters.eq("_id", new ObjectId(id))).first();
        Producto producto = new Producto();
        producto.setProducto(documentProducto.getString("producto"));
        producto.setStock(documentProducto.getInteger("stock"));
        producto.setValor(documentProducto.getInteger("valor"));
        producto.set_id(documentProducto.getObjectId("_id"));
        return producto;
    }
    
    public Producto addProducto(Producto producto){
        Document documentProducto = new Document()
            .append("producto", producto.getProducto())
            .append("stock", producto.getStock())
            .append("valor", producto.getValor());
        
        ObjectId insertedId = getCollection().insertOne(documentProducto).getInsertedId().asObjectId().getValue();
        producto.set_id(insertedId);
        return producto;
    }

    private MongoCollection getCollection() {
        return mongo.getDatabase(database).getCollection(collection);
    }
}
