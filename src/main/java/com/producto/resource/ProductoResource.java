package com.producto.resource;

import com.producto.model.Producto;
import com.producto.service.ProductoService;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/")
public class ProductoResource {
    
    @Inject
    @Singleton
    ProductoService service;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Producto> getProductos(){
        return service.listAllProductos();
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Producto getProductoById(@PathParam("id") String id){
        return service.getProductoById(id);
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Producto postProducto(Producto producto){
        return service.addProducto(producto);
    }
}
