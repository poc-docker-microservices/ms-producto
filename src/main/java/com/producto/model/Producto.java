package com.producto.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.bson.types.ObjectId;

public class Producto {
    
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId _id;
    private String producto;
    private Integer stock;
    private Integer valor;
    
    public Producto() {
    }

    public Producto(ObjectId _id, String producto, Integer stock, Integer valor) {
        this._id = _id;
        this.producto = producto;
        this.stock = stock;
        this.valor = valor;
    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }
    
}
